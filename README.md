# Clusterhat Panel Alpha

### Demo
https://blog.jmdawson.co.uk:8002

### Prereq's
* SSH Must Be enabled on all Pi's connected to the clusterhat
* The base image from clusterhat.net must be installed to the pi's

## Install 
Run the install.sh script on the controller and follow the instructions carefully. 
`./install.sh`

## Dependencies 
The following packages are installed.
### Controller
PHP 7.0
### Pi Zeros
tightvncserver
x11
xterm

This is all - No warranty is provided and expect bugs, there are lots of "Hacks" That will be fixed as this progresses.

# Todo
Add authentication 
Tidy up code

